<?php

use App\User;
use App\News;
use App\Tag;
use League\Fractal;
use Illuminate\Routing\RouteGroup;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['prefix' => 'news'], function () {
    Route::get('/', 'NewsController@index')->name('news.view');
    Route::get('/addnews', 'NewsController@create')->name('news.addnews');
    Route::get('/{id}', 'NewsController@show')->name('news.view2/');
    Route::post('/savenews', 'NewsController@store')->name('news.create');
    Route::get('edit/{id}', 'NewsController@edit')->name('news.update');
    Route::post('/{id}', 'NewsController@update')->name('news.update');
    Route::get('del/{id}', 'NewsController@destroy')->name('delnews');

  
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/authors/{author}', function (User $author) {
    return view('getnews', [
        'news' => $author->news
    ]);
});


Route::group(['prefix' => 'tag'], function () {
    Route::get('/', 'TagController@index')->name('news.tags');
    Route::get('/{tag}', 'TagController@show');
    Route::post('/savetags', 'TagController@store');
});

Route::group(['prefix' => 'api'], function () {
    Route::get('/news', 'APIUserListController@newslistall'); 
    Route::get('/news', 'APIUserListController@newslist');  
    Route::get('/user', 'APIUserListController@userlist'); 
    Route::get('/tag', 'APIUserListController@taglist');  
});