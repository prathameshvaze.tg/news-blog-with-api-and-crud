@extends('layouts.app')

@section('content')
<link href="{{ asset('/css/allnews.css') }}" rel="stylesheet">
<div class="container">
    @foreach($news as $n)

    <article class="article">
        <div class="title-area">
            <h1>
                <a href="/news/{{ $n->id }}">
                    {{ $n->title}}
                </a>
            </h1>
            <div>
                <button class="btn btn-danger">
                    <a href="/news/del/{{$n->id}}" class="delete">
                        Delete
                    </a>
                </button>
                <button class="btn btn-warning ">
                    <a href="/news/edit/{{$n->id}}" class="update">
                        Update
                    </a>
                </button>

            </div>
        </div>


        <strong>by <a href="/authors/{{$n->author->id}}">{{$n->author->name}}</a></strong>
        <h3>
            <i>{{ $n->short_description}}</i>
        </h3>

        @foreach($n->tag as $tag)

        <button class="btn btn-primary tag">
            <a href="/tag/{{$tag->id}}" class="tags">
                {{$tag->name}}
            </a>
        </button>
        @endforeach

    </article>
    @endforeach
</div>
@endsection