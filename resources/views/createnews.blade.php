@extends('layouts.app')

@section('content')
<div class="container">
  <!-- @if ($message = Session::get('success'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
                <strong>{{ $message }}</strong>
        </div>
        <img src="uploads/{{ Session::get('file') }}">
        @endif
  
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif -->
</div>
<div class="container">
  <form action="/news/savenews" method="post" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
      <label for="title">Title</label>
      <input type="text" class="form-control" id="title" placeholder="Enter News title" name="title">
    </div>

    <div class="form-group">
      <label for="author">Select Author</label>
      <select class="form-select" aria-label="Default select example" name="author_id">
        <option selected>Select Author</option>
        @foreach($authors as $author)
        <option value="{{ $author->id }}">{{$author->name}}</option>
        @endforeach
      </select>
    </div>

    <div class="form-group">
      <label for="tags">Select Tags</label>
      <select class="form-select" aria-label="Default select example" name="tags_id[]" multiple>
        <option selected>Select Tags</option>
        @foreach($tags as $tag)
        <option value="{{ $tag->id }}">{{$tag->name}}</option>
        @endforeach
      </select>
    </div>

    <div class="form-group">
      <label for="short_desc">Short Description</label>
      <input type="text" class="form-control" id="short_desc" placeholder="Short Description" name="shortdesc">
    </div>
    <div class="form-group">
      <label for="short_desc">Long Description</label>
      <textarea class="form-control" id="long_desc" placeholder="Long Description" name="longdesc" rows="5"></textarea>
    </div>
    <div class="form-group">
      <label for="short_desc">Video Link</label>
      <input type="text" class="form-control" id="videolink" placeholder="Video Link" name="videolink">
    </div>

    <div class="form-group">
      <label for="header">Header Image</label>
      <input type="file" class="form-control-file" id="header" name="header">
    </div>

    <div class="form-group">
      <label for="postdoc">Post document</label>
      <input type="file" class="form-control-file" id="postdoc" name="postdoc">
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
  </form>
</div>
@endsection