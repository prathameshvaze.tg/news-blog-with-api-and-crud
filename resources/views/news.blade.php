@extends('layouts.app')

@section('content')
<link href="{{ asset('/css/news.css') }}" rel="stylesheet">
<div class="container">
    <article>
        <h1>

            {{$news->title}}
        </h1>
        <strong>by <a href="/authors/{{$news->author->id}}">{{$news->author->name}}</a></strong>
        <div>
            {{ $news->short_description}}
        </div>
        <div>
            {{ $news->long_description}}
        </div>
        <div>
            @foreach($news->tag as $tag)

            <button class="btn btn-primary tag">
                <a href="/tag/{{$tag->id}}" class="tags">
                    {{$tag->name}}
                </a>
            </button>
            @endforeach
        </div>
        <!-- <a href="/allposts">Go back</a> -->
        <a href="/news">Back</a>
    </article>
</div>
@endsection