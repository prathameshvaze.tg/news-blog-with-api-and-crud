@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header"> newsletter managemant</div>
        <div class="card-body">
            <form action="/tag/savetags" method="post">
                @csrf
                <input type="text" name="tags" class="form-control" placeholder="Enter Tags"><br>
                <input type="submit" value="Add" class="btn btn-primary">
            </form>
        </div>
    </div>
</div>
</div>
<div class="container">
    <table class="table">
        <thead>
            <tr>
                <th scope="col">Tag names</th>
            </tr>
        </thead>
        <tbody>
            @foreach($tags as $tag)
            <tr>
                <td>
                    {{$tag->name}}
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection