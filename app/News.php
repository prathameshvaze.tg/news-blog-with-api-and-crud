<?php

namespace App;

use App\Tag;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = [
        'title', 'author_id', 'short_description' , 'long_description', 'post_document' , 'header_image_mini', 'header_image' ,'video_link'
    ];

    public function tag()
    {
        return $this->belongsToMany(Tag::class,'news_tag_pivots')->withTimestamps();
    }

    public function author(){
        return $this->hasOne(User::class,'id','author_id');
    }
}
