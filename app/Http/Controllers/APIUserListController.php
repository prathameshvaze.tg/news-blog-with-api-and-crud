<?php

namespace App\Http\Controllers;

use App\Transformers\NewsTransformer;
use App\Transformers\NewsDetailsTransformer;
use App\Transformers\AuthorTransformer;
use App\Transformers\TagTransformer;
use App\News;
use App\User;
use App\Tag;
// use League\Fractal;
// use League\Fractal\Manager;
// use League\Fractal\Resource\Collection as Collection;
// use League\Fractal\Resource\Item as Item;
// use League\Fractal\Pagination\IlluminatePaginatorAdapter;
// use League\Fractal\Serializer\ArraySerializer;
// use PDF;
// use Spatie\Fractal\Fractal;


class APIUserListController extends Controller
{
    public function newslistall()
    {
        // $newslist = News::all()->transformWith(new NewsTransformer())->toJson();
        // return $newslist;
        //dd($news);
        // $fractal = new Manager();
        // $fractal->parseIncludes('author ,tag');
        // $resource = new Collection($news, new NewsTransformer);
        // return $fractal->createData($resource)->toJson();
        // $tag = Tag::all();
        // $author = User::all();
        $news = News::all();
        return fractal()
            ->collection($news)
            ->parseIncludes('author', 'tag')
            ->transformWith(new NewsDetailsTransformer())
            ->toArray();
    }

    public function newslist()
    {
        $newslist = News::all()->transformWith(new NewsTransformer())->toArray();
        return $newslist;
    }

    public function userlist(User $author)
    {
        $authorlist = User::all()->transformWith(new AuthorTransformer())->toJson();

        return $authorlist;
    }

    public function taglist(Tag $tag)
    {
        $newslist = Tag::all()->transformWith(new TagTransformer())->toJson();

        return $newslist;
    }
}
