<?php

namespace App\Http\Controllers;

use Intervention\Image\ImageManager;
use App\Tag;
use App\News;
use App\User;
use App\NewsTagPivot;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class NewsController extends Controller
{
    // $news = News::latest()->with('tag','author')->get();
    // dd($news);
    // // $authors = User::get();
    // // 

    public function index()
    {
        return view('getnews', [
            'news' => News::latest()->with('tag', 'author')->get()
        ]);
    }

    public function create(Request $request)
    {
        $authors = User::get();
        $tags = Tag::get();
        return view('createnews', compact('authors', 'tags'));
    }


    public function store(Request $request)
    {
        $request->validate([
            'author_id' => 'required',
            'tags_id' => 'required',
            // 'header' =>  'required|mimes:jpeg,png,jpg,pdf|max:1024',
            // 'postdoc' => 'required|mimes:jpeg,png,jpg,pdf|max:1024',
            'title' => 'required|max:255',
            'shortdesc' => 'required|max:255',
            'longdesc' => 'required',
            'videolink' => 'required|max:255'
        ]);

        $input1['title'] = $request->title;
        $input1['author_id'] = $request->author_id;
        $input1['short_description'] = $request->shortdesc;
        $input1['long_description'] = $request->longdesc;
        $input1['video_link'] = $request->videolink;
        $news = News::create($input1);

        $news->tag()->attach($request->tags_id);
        $news->save();

        return redirect('/news/addnews');
    }

    public function show($id)
    {
        return view('news', [
            'news' => News::findOrFail($id)
        ]);
    }

    public function edit($id)
    {
        $authors = User::get();
        $tags = Tag::get();
        $news = \App\News::find($id);
        return view('edit', compact('news', 'id', 'authors', 'tags'));
    }

    public function update(Request $request, $id)
    {
        $news = \App\News::find($id);
        $news->title = $request->get('title');
        $news->short_description = $request->get('shortdesc');
        $news->long_description = $request->get('longdesc');
        $news->video_link = $request->get('videolink');
        $news->save();
        return redirect('/news');
    }

    public function destroy($id)
    {
        $news = \App\News::find($id);
        $news->delete();
        return redirect('/news')->with('success', 'Information has been  deleted');
    }
}
