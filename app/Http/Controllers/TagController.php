<?php

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;

class TagController extends Controller
{
    public function index()
    {
        
        $tags = Tag::get();
        return view('createtags', compact('tags'));
    }

    public function create(Request $request)
    {
        
    }

    public function store(Request $request)
    {
        // return view('createtag', compact('tag'));
        $input['name'] = $request->tags;
        Tag::create($input);
        return redirect('/crop');
    }

    public function show($id)
    {
        $tag = Tag::findOrFail($id);
        return view('getnews', [
            'news' => $tag->news
        ]);
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
