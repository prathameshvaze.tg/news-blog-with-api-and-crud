<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsTagPivot extends Model
{
    protected $fillable = [
        'tag_id',
        'news_id',
    ];
}
