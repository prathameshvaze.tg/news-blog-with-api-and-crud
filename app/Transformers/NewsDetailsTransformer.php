<?php

namespace App\Transformers;

use App\News;
use App\Transformers\TagTransformer;
use App\Transformers\AuthorTransformer;

use League\Fractal\TransformerAbstract;

class NewsDetailsTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
    'author',
    'tag'
    ];

    // protected $availableIncludes = [ 
    //     'author',
    //     'tag'
    // ];

    public function transform(News $news)
    {
        return [
            'news-id' => (int) $news->id,
            'news-title' => $news->title,
            'news-short-desc' => $news->short_description,
            'news-long-desc' => $news->long_description,
            'news-video-link' => $news->video_link,
        ];
    }


    public function includeAuthor(News $news)
    {
        $author = $news->author;
        return $this->collection($author, new AuthorTransformer);
    }

    public function includeTag(News $news)
    {
        $tag = $news->tag;
        return $this->collection($tag, new TagTransformer);
    }
}