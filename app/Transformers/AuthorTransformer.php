<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class AuthorTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $author)
    {
        return [
            'author-id' => $author->id,
            'author-name' => $author->name,
            'author-email' => $author->email,
            'author-username' => $author->twitter,
        ];
    }
}
